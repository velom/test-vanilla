import {
    createElement,
    on,
    getRandomInt,
    htmlDecode,
    getRandomDate
} from '../utils/index';

import {
    Button,
    Input,
    Form,
    List,
    Rating,
} from '../components/index';
import {
    SEARCH_MODULE,
    btnConfig,
    inputConfig,
    formConfig,
    listConfig,
    searchModuleMediator,
} from './searchModuleConfig';

import './searchModuleStyles.css';

const SearchModuleButton = Button(btnConfig);
const SearchModuleInput = Input(inputConfig);
const SearchModuleForm = Form(formConfig)([SearchModuleInput, SearchModuleButton]);
const SearchModuleList = List(listConfig);
const SearchBlock = createElement({
    tagName: "div",
    className: `${SEARCH_MODULE}__block`,
    attributes: {
        id: SEARCH_MODULE
    },
    children: [
        SearchModuleForm,
        SearchModuleList
    ]
})

const EVENTS = {
    SET_STORAGE_DATA: 'setStorageData',

    LOAD_JSON_DATA: 'loadJsonData',
    ON_INPUT_CHANGE: 'onInputChange',
    ON_FORM_SUBMIT: 'onFormSubmit',

    SHOW_RESULTS: 'showResults',

    MOUNT_LIST: 'mountList',
    UN_MOUNT_LIST: 'unMountList',
}

const storageInitData = {
    listIsActive: null,
    allItems: null,
    filteredItems: null,
    searchValue: null
};

searchModuleMediator.registerEvent(EVENTS.SET_STORAGE_DATA, function (field, data, callBack = e => e) {

    if (typeof field === 'string') {
        if (!searchModuleMediator.storage[field]) searchModuleMediator.storage[field] = null;
        searchModuleMediator.storage[field] = data;
        callBack(searchModuleMediator);
        return searchModuleMediator;
    }

    if (typeof field === "object" && !Array.isArray(field) && field !== null && data === null) {
        for (let key in field) {
            searchModuleMediator.storage[key] = field[key];
        }
        return searchModuleMediator;
    }

})

searchModuleMediator.registerEvent(EVENTS.LOAD_JSON_DATA, async function () {
    try {
        const fakeDataRequest = await fetch('fakeData.json');
        const fakeDataRequestToJson = await fakeDataRequest.json();
        searchModuleMediator.callEvent(EVENTS.SET_STORAGE_DATA, 'allItems', fakeDataRequestToJson.tours)
    } catch (e) {
        throw new Error(e);
    }
});

searchModuleMediator.registerEvent(EVENTS.ON_INPUT_CHANGE, function (evt) {
    searchModuleMediator.callEvent(EVENTS.SET_STORAGE_DATA, 'searchValue', evt.target.value);
});

searchModuleMediator.registerEvent(EVENTS.SHOW_RESULTS, function (filteredItems) {

    if (!filteredItems || !filteredItems.length) {
        return searchModuleMediator.callEvent(EVENTS.UN_MOUNT_LIST, searchModuleMediator.storage.filteredItems)
    }

    return searchModuleMediator.callEvent(EVENTS.MOUNT_LIST, searchModuleMediator.storage.filteredItems)
});

searchModuleMediator.registerEvent(EVENTS.ON_FORM_SUBMIT, function (evt) {
    evt.preventDefault();
    const value = searchModuleMediator.storage.searchValue;

    if (value === '') return searchModuleMediator.callEvent(EVENTS.UN_MOUNT_LIST);

    const regEx = new RegExp(value, 'gi');
    const filteredItems = searchModuleMediator.storage.allItems.filter((el, idx, arr) => el['title'].match(regEx));
    searchModuleMediator.callEvent(
        EVENTS.SET_STORAGE_DATA,
        'filteredItems',
        filteredItems,
        () => searchModuleMediator.callEvent(EVENTS.SHOW_RESULTS, searchModuleMediator.storage.filteredItems)
    );
})

searchModuleMediator.registerEvent(EVENTS.MOUNT_LIST, function () {
    SearchBlock.appendChild(SearchModuleList);
    SearchModuleList.innerHTML = "";
    const fragment = document.createDocumentFragment();
    const searchText = searchModuleMediator.storage.searchValue;
    const searchTextRegExp = new RegExp(searchText, "i");

    fragment.appendChild(createElement({
        tagName: 'li',
        html: `
        <div class="${SEARCH_MODULE}__list_item_head">Search results for - 
            <span class="${SEARCH_MODULE}__list_item_head_hint">
                ${searchModuleMediator.storage.searchValue}
            </span>
            <div class="${SEARCH_MODULE}__list_item_head_hint_count">Results found ${searchModuleMediator.storage.filteredItems.length}</div>
        </div>`,
        className: `${SEARCH_MODULE}__list_item ${SEARCH_MODULE}__list_item--title`,
    }))

    searchModuleMediator.storage.filteredItems.forEach((element, idx, arr) => {

        const {
            rating,
            title,
            currency,
            price,
            isSpecialOffer
        } = element;

        const highlightedTitle = htmlDecode(title).replace(searchTextRegExp, `<span class="${SEARCH_MODULE}__list_item_selection">$&</span>`);
        const listItemClass = isSpecialOffer ? `${SEARCH_MODULE}__list_item ${SEARCH_MODULE}__list_item--special` : `${SEARCH_MODULE}__list_item`;
        const listItemPriceClass = isSpecialOffer ? `${SEARCH_MODULE}__list_item_price ${SEARCH_MODULE}__list_item_price--special` : `${SEARCH_MODULE}__list_item_price`;
        const listItemSpecialOfferClass = isSpecialOffer ? `${SEARCH_MODULE}__list_item_special_offer ${SEARCH_MODULE}__list_item_special_offer--active` : `${SEARCH_MODULE}__list_item_special_offer`

        const listItem = createElement({
            tagName: 'li',
            className: listItemClass,
            children: [{
                tagName: 'div',
                className: `${SEARCH_MODULE}__list_item_content`,
                children: [{
                        tagName: 'div',
                        className: `${SEARCH_MODULE}__list_item_image_block`,
                        children: [{
                            tagName: 'img',
                            className: `${SEARCH_MODULE}__list_item_image`,
                            attributes: {
                                src: `https://placeimg.com/150/150/${getRandomInt(1,1000)}`,
                                alt: element.title,
                                width: 80,
                                height: 80,
                            },
                        }, ]
                    },
                    {
                        tagName: 'div',
                        className: `${SEARCH_MODULE}__list_item_info_block`,
                        children: [{
                                tagName: 'div',
                                className: `${SEARCH_MODULE}__list_item_location`,
                                text: 'Berlin',
                            },
                            {
                                tagName: 'div',
                                className: `${SEARCH_MODULE}__list_item_title`,
                                html: highlightedTitle,
                            },
                            {
                                tagName: 'div',
                                className: `${SEARCH_MODULE}__list_item_date`,
                                text: `From ${getRandomDate(new Date(2012, 0, 1), new Date()).toISOString().slice(0, 10)}`
                            }, {
                                tagName: 'div',
                                className: listItemPriceClass,
                                text: `${currency} ${price}`,
                                children: [{
                                    tagName: 'div',
                                    className: listItemSpecialOfferClass,
                                    text: 'Special Offer'
                                }]
                            }, {
                                tagName: 'div',
                                className: `${SEARCH_MODULE}__list_item_rating`,
                                children: [
                                    Rating(Math.ceil(rating)),
                                    {
                                        tagName: 'span',
                                        className: `${SEARCH_MODULE}__list_item_reviews`,
                                        text: `${getRandomInt(1,200)} reviews`,
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }]
        });

        fragment.appendChild(listItem)
    });

    SearchModuleList.appendChild(fragment);
    searchModuleMediator.callEvent(EVENTS.SET_STORAGE_DATA, 'listIsActive', true)
});
searchModuleMediator.registerEvent(EVENTS.UN_MOUNT_LIST, function () {

    if (searchModuleMediator.storage.listIsActive === null || searchModuleMediator.storage.listIsActive) {
        SearchModuleList.parentNode.removeChild(SearchModuleList);
        searchModuleMediator.callEvent(EVENTS.SET_STORAGE_DATA, 'listIsActive', false);
    }

});

searchModuleMediator.callEvent(EVENTS.SET_STORAGE_DATA, storageInitData, null);

on.call(SearchModuleInput, "input", (evt) => searchModuleMediator.callEvent(EVENTS.ON_INPUT_CHANGE, evt));
on.call(SearchModuleForm, "submit", (evt) => searchModuleMediator.callEvent(EVENTS.ON_FORM_SUBMIT, evt));


const SearchModule = (function () {
    return {
        init: function (container) {
            container.appendChild(SearchBlock);
            searchModuleMediator.callEvent(EVENTS.LOAD_JSON_DATA);
            searchModuleMediator.callEvent(EVENTS.UN_MOUNT_LIST);
        }
    }
})();

export default SearchModule;