import {
    EventBus
} from '../utils/index';

const SEARCH_MODULE = 'searchModule';

const btnConfig = {
    className: `${SEARCH_MODULE}__btn`,
    id: `${SEARCH_MODULE}Btn`,
    type: 'submit',
    html: ''
}
const inputConfig = {
    className: `${SEARCH_MODULE}__input`,
    id: `${SEARCH_MODULE}Input`,
    type: 'text',
    placeholder: 'Search tours...',
    autocomplete: "off"
}

const formConfig = {
    wrapClassName: `${SEARCH_MODULE}__form_wrap`,
    className: `${SEARCH_MODULE}__form`,
    action: '#',
    id: `${SEARCH_MODULE}Form`,
}
const listConfig = {
    className: `${SEARCH_MODULE}__list`,
    id: `${SEARCH_MODULE}List`,
}

const searchModuleMediator = new EventBus();
searchModuleMediator.storage = {};

export {
    SEARCH_MODULE,
    btnConfig,
    inputConfig,
    formConfig,
    listConfig,
    searchModuleMediator
}