const htmlDecode = innerHTML => Object.assign(document.createElement('textarea'), {
    innerHTML
}).value;

export default htmlDecode;