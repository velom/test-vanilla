function on(eventName, eventHandler, dir = false) {
    return this.addEventListener(eventName, eventHandler, dir);
}

function off(eventName, eventHandler) {
    return this.removeEventListener(eventName, eventHandler);
}

export {
    on,
    off
}