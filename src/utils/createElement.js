function createElement(options = {}) {
    let el, a, i;
    if (!options.tagName) {
        el = document.createDocumentFragment()
    } else {
        el = document.createElement(options.tagName)
        if (options.className) {
            el.className = options.className
        }

        if (options.attributes) {
            for (a in options.attributes) {
                el.setAttribute(a, options.attributes[a])
            }
        }

        if (options.html !== undefined) {
            el.innerHTML = options.html
        }
    }

    if (options.text) {
        el.appendChild(document.createTextNode(options.text))
    }

    // IE 8 doesn"t have HTMLElement
    if (window.HTMLElement === undefined) {
        window.HTMLElement = Element
    }

    if (options.children && options.children.length) {
        for (i = 0; i < options.children.length; i++) {
            el.appendChild(options.children[i] instanceof window.HTMLElement ? options.children[i] : createElement(options.children[i]))
        }
    }

    return el;
}

// document.body.appendChild(createElement({
//     tagName: "div",
//     className: "my-class",
//     text: "Blah blah",
//     attributes: {
//         "id": "element id",
//         "data-truc": "value"
//     },
//     children: [{ /* recursif call **/ }]
// }))


export default createElement;