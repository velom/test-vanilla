
const getArrayOfElements = elements =>[...Array(elements)].map((_, i) => ++i);

export default getArrayOfElements;