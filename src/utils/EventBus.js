class EventBus {
    constructor() {
        this.events = {}
    }
    registerEvent(name, fn) {
        if (!this.events[name]) this.events[name] = [];
        this.events[name].push(fn);
        return this;
    }
    callEvent(name, ...args) {
        if (!this.events[name]) throw new Error('This event doesn\'t exist');
        this.events[name].forEach(fn => {
            fn.apply(null, [...args]);
        });
        return this;
    }
}

export default EventBus;