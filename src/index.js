import SearchModule from './SearchModule/index';
import {
    documentReady
} from './utils/index';
import './styles/index.css'


const container = document.querySelector('#root');

const letTheMagicBegin = () => {
    SearchModule.init(container)
}

documentReady(
    letTheMagicBegin()
);