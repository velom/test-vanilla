import {
    createElement
} from '../../utils/index'

const List = function ({
    className,
    id,
    children = []
}) {
    return createElement({
        tagName: "ul",
        className,
        attributes: {
            id
        },
        children
    })

};

export default List;