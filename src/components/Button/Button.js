import {
    createElement
} from '../../utils/index';


const Button = function ({
    className,
    text,
    id,
    type,
    html
}) {
    return createElement({
        tagName: "button",
        className,
        text: text ? text : '',
        html: html ? html : '',

        attributes: {
            id,
            type
        }
    });
};

export default Button;