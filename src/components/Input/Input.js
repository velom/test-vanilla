import {
    createElement
} from '../../utils/index'

const Input = function ({
    id,
    className,
    placeholder,
    type,
    autocomplete
}) {
    const input = createElement({
        tagName: "input",
        className,
        attributes: {
            id,
            placeholder,
            type,
            autocomplete,
        }
    })

    return input;
};

export default Input;