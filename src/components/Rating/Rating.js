import {
    createElement
} from "../../utils/index";
import {
    getArrayOfElements
} from '../../utils/index';
import './Rating.css';

const Rating = (dataStars) => {

    const star = (height = 25, width = 23, dataRating = 1, className = 'star rating') => `
    <svg height="${height}" width="${width}" data-rating="${dataRating}" class="${className}">
       <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" style="fill-rule:nonzero;" />
    </svg>
    `;
    [...Array(5)]
    .map((_, i) => ++i)
    const stars = getArrayOfElements(5)
        .map(el => star(25, 23, el, "star rating"))
        .join('');


    return createElement({
        tagName: "div",
        className: 'stars',
        attributes: {
            "data-stars": dataStars
        },
        html: stars
    });

};

export default Rating;