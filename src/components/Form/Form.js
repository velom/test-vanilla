import {
    createElement
} from '../../utils/index'

const Form = function ({
    id,
    className,
    action,
    children,
    wrapClassName
}) {
    return function (children=[]) {
        
        return createElement({
            tagName:'div',
            className: wrapClassName,
            children:[{
                tagName: "form",
                className,
                attributes: {
                    id,
                    action
                },
                children
            }]
            
        })
    }

};

export default Form;